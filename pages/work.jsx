import Hero from '@/components/Hero'
import Portfolio from '@/components/Portfolio'
import React from 'react'

const work = () => {
  return (
    <div>
        <Hero heading='My Work' message='Explore the world through my lens'/>
        <Portfolio />
    </div>
  )
}

export default work