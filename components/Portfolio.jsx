import Image from 'next/image'
import React from 'react'

const Portfolio = () => {
  return (
    <div className='max-w-[1240px] mx-auto py-16 text-center'>
        <h1 className='font-bold text-2xl p-4'> Travel Photos</h1>
        <div className='grid grid-rows-none md:grid-cols-5 p-4 gap-4'>
            <div className='w-full h-full col-span-2 md:col-span-3 row-span-2'>
                <Image src='https://images.pexels.com/photos/161815/santorini-oia-greece-water-161815.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'
                width='677'
                height='451'
                 />
            </div>
            <div className='w-full h-full'>
            <Image src='https://images.pexels.com/photos/534351/pexels-photo-534351.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' 
            width='215'
            height='217'/>
            </div>
            <div className='w-full h-full'>
            <Image src='https://images.pexels.com/photos/1486222/pexels-photo-1486222.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' 
            width='215'
            height='217'/>
            </div>
            <div className='w-full h-full'>
            <Image src='https://images.pexels.com/photos/3458997/pexels-photo-3458997.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' 
            width='215'
            height='217'/>
            </div>
            <div className='w-full h-full'>
            <Image src='https://images.pexels.com/photos/11567961/pexels-photo-11567961.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' 
            width='215'
            height='217'/>
            </div>
            
         
        </div>
    </div>
  )
}

export default Portfolio